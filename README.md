1. Create sub-user on account

```
$ triton rbac user -a
login: deploy
password: ... (not actually used)
email: deploy@xlex
firstName: Deploy
lastName: User
companyName: 
address: 
postalCode: 
city: 
state: 
country: 
phone:

Created user "deploy"
```

2. Create policy

```
$ triton rbac policy -a
name: deployment
description: 
Enter one rule per line. Enter an empty rule to finish rules.
rule: can execmachinecommand * when tag_deploy::boolean = true
rule: can getmachine * when tag_deploy::boolean = true
rule: can listmachines *
rule: can getdirectory /xlex/stor/builds
rule: can getobject /xlex/stor/builds/*
rule: can putobject /xlex/stor/builds/* and /xlex/stor/builds
rule: 

Created policy "deployment"
```

3. Create role

```
$ triton rbac role -a
name: deployment
members:
  - type: subuser
    default: true
    login: deploy
policies:
  - name: deployment

Created role "deployment"
```

4. Tag any zones/VMs that it needs to access

```
$ triton inst tag set foobar deploy=true
$ triton inst tag set wp346test deploy=true
```

5. Make bitbucket generate an SSH key for this repo, and add the public key
to the subuser

(Use `Repository settings` -> `SSH keys` and generate it. Only need the public half).

```
$ triton rbac key -a deploy -
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCJmbaM0VnO5Hh8YN0azp28OMAYxA75gp2aGEe0TsUc9hsANiL24azS7+CS7TvMxK6GU55AyxSTJxvGQ5EzcxXlB8N++wHdpi3RmDgwbc9F3qoa8xF5AIRBRSLNcuyjfVJHtEGKbN2YMIQ1oOUJDeH5C1ckKDo8+UW4dEbQpCkCeBoTjYT48JLD110ckhuFNBAoho9ivG2hDNoh0evhN225f71KezlqRBwbjikR8yfM1WbYOW9VbvfYBNomDPuh9pndj3rUdlilMW75hxRktO0QPCqkwy9HoNItjDogLDSv+rB3+9mkUmwqq1iUyW/h2AJyzPvpi1F4TLqzhEvcPR4XTRZkljgl3nLFH3wy8OWfP+v47aEo/YE32bRvqTKutsLePVw5iwkRcXTJBjM8W2Fv2zpw+0n5WgdnOLRren7S9dPzUaQB2qthyPEhprPycK011ubfuScYPSCQtF9JXw9MOgCv29qzBN6VtwqSWNhtAgiXGSrqCudCgmwsh0/1rLs=
^D
Added user deploy key "f5:fb:2f:2b:95:c4:13:4f:6f:b3:00:d3:9c:1f:e5:64" ((unnamed))
```

6. Adjust the key fingerprint and account in `.triton.json`, then add commands
for actual deployment etc.

