prefix ?= /usr
bindir ?= $(prefix)/bin

.PHONY: install
install:
	install -o root -g root -d $(destdir)$(bindir)
	install -o root -g root -m 0755 thing $(destdir)$(bindir)
